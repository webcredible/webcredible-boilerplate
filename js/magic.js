/*SMOOTH SCROLL*/

$(document).ready(function(){

	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash,
	    $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 1000, 'swing', function () {
	        window.location.hash = target;
	    });
	});
});

/*SHOW SCREEN SIZE*/

$(document).ready(function(){
	$(window).on('load', showSize);
	$(window).on('resize', showSize);
	showSize();
	function showSize() {
	$('#width').html(/*'HEIGHT:'+$(window).height()+*/' WIDTH '+$(window).width()+'px');
	}
});

/*DIALOG BOX*/

$(document).ready(function() {
	$('.dialog').hide();

    $('.open-dialog').click(function() {
    	$('.container').addClass('dimmed');
        $('.overlay').css('display', 'block');
        $('.dialog').fadeToggle(250);
    });

    $('.close-dialog').click(function() {
    	$('.container').removeClass('dimmed');
       	$('.overlay').css('display', 'none');
       	$('.dialog').toggle();
    });
});

